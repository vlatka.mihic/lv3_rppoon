﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_RPPOON
{
    interface IExample
    {
        System.String Name { get; }
        void Run();
    }
}

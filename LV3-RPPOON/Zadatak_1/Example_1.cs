﻿using System;
using System.Collections.Generic;
using System.IO;

/*
Izmijeniti klasu iz primjera 1.2 tako da umjesto plitkog vrši duboko (engl. deep) kopiranje. Za potrebe
testiranja napraviti malenu CSV datoteku po uzoru na primjer u nastavku. Je li potrebno duboko kopiranje za
klasu u pitanju?

    U ovome slučaju bilo bi u redu i plitko kopiranje jer podatke učitavamo iz datoteke i nemamo metode kojima 
    možemo izmjeniti podatke, jedina metoda koja bi mogla predstavljati problem kod plitkog kopiranja je metoda
    ClearData() zbog toga što kopija kod plitkog kopiranja sadrži samo referencu na podatke te ukoliko 
    izbrišemo podatke iz originalnog objekta ti podaci više neće biti dostupni kopijama tog objekta. 
*/


namespace LV3_RPPOON.Zadatak_1
{
    class Example_1: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Print(IList<List<string>> data)
        {
            foreach (var info in data)
            {
                foreach (var inf in info)
                {
                    Console.WriteLine(inf);
                }
            }
        }
        public void Run()
        {
            Dataset dataset = new Dataset("C:\\Users\\Vlatka\\source\\repos\\LV3-RPPOON\\LV3-RPPOON\\LV3-RPPOON\\bin\\Debug\\netcoreapp3.1\\LV3.txt");
            Print(dataset.GetData());
            Dataset prototype = dataset.Clone() as Dataset;
            dataset.ClearData();
            Print(prototype.GetData());
        }
    }
}

﻿using System;

/*
Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?

    U konkretnom slučaju nema potrebe raditi duboko kopiranje jer se objekt klase ConsoleNotification ne može mijenjati izvana. 
    Stoga nema razlike između plitkog i dubokog kopiranja u ovom konkretnom slučaju.
    Do mijenjanja objekta klase ConsoleNotification ne može doći zbog toga što su svi seteri private i nema pristupnih metoda, nema metoda za 
    brisanje stanja unutar objekta ili nešto tome slično.
 */

namespace LV3_RPPOON.Zadatak_7
{
    public enum Category { ERROR, ALERT, INFO }
    class Example_7: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }
        public void Run()
        {
            String author = "Vlatka Mihic";
            String title = "LV3-Zadatak4";
            String text = "Klasu ConsoleNotification iz primjera 3 izmijeniti tako da ugrađuje sučelje Protoype iz primjera 1.2. ";
            DateTime timestamp = DateTime.Now;
            Category level = Category.INFO;
            ConsoleColor color = ConsoleColor.Green;

            ConsoleNotification consoleNotification = new ConsoleNotification(author, title, text, timestamp, level, color);
            Display(consoleNotification);

            ConsoleNotification prototype = consoleNotification.Clone() as ConsoleNotification;
            Display(prototype);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON.Zadatak_7
{
    interface Prototype
    {
        Prototype Clone();
    }
}

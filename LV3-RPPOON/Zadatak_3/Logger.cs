﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON.Zadatak_3
{
    class Logger
    {
        private static Logger instance;
        public string filePath { get; set; }

        private Logger()
        {
            this.filePath = "C:\\Users\\Vlatka\\source\\repos\\LV3-RPPOON\\LV3-RPPOON\\LV3-RPPOON\\bin\\Debug\\netcoreapp3.1\\LV3_Zad3.txt";
        }
        public static Logger GetInstance()
        {
            if(instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string log)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(log);
            }
        }

    }
}

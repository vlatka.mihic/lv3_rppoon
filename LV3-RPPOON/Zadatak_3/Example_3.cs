﻿using System;

/*
Ako je datoteka postavljena na jednom mjestu u tekstu programa, hoće li uporaba loggera na
drugim mjesta u testu programa pisati u istu datoteku (pretpostavka je kako nisu ponovo postavljene)?

    Hoce, jer je putanja na datoteku u koju ispisujemo podatke spremljena unutar objekta logger te je na taj način potrebno najprije 
    ponovno postaviti tu vrijednost na neku drugu datoteu kako bi se ispisala na drugim mjestima.
*/

namespace LV3_RPPOON.Zadatak_3
{
    class Example_3: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            Logger logger = Logger.GetInstance();

            logger.Log("Naslov dokumenta: ");
            logger.Log("Razvoj programske podrške objektno orijentiranim načelima");

            logger.filePath = "C:\\Users\\Vlatka\\source\\repos\\LV3-RPPOON\\LV3-RPPOON\\LV3-RPPOON\\bin\\Debug\\netcoreapp3.1\\LV3_Zad3_new.txt";

            string log = Console.ReadLine();

            logger.Log(log);
        }
    }
}

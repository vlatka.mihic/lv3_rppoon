﻿using System;

/*
 Napisati konkretnu klasu NotificationBuilder koja implementira sučelje IBuilder. Klasa mora omogućiti sve
metode koje propisuje sučelje, a sama treba brinuti o čuvanju privremenih vrijednosti sve dok objekt
ConsoleNotification nije izgrađen kao i o podrazumijevanim vrijednostima istih. Taj se objekt izgrađuje
metodom Build() korištenjem parametarskog konstruktora klase čiji se objekt izgrađuje. 
*/

namespace LV3_RPPOON.Zadatak_5
{
    public enum Category { ERROR, ALERT, INFO }
    class Example_5: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }
        public void Run()
        {
            NotificationBuilder builder = new NotificationBuilder();

            builder.SetAuthor("Vlatka");
            builder.SetColor(ConsoleColor.DarkMagenta);
            builder.SetText("Laboratorijske vjezbe iz RPPOON-a: 23.04.2020. u 8:00h !!");

            ConsoleNotification notification = builder.Build();

            Display(notification);
        }
    }
}

﻿using System;

namespace LV3_RPPOON.Zadatak_6
{
    public enum Category { ERROR, ALERT, INFO }
    class Example_6: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Display(ConsoleNotification notification)
        {
            Console.ForegroundColor = notification.Color;
            Console.Write(notification.Author + ": ");
            Console.WriteLine(notification.Title);
            Console.WriteLine(notification.Timestamp.ToString());
            Console.WriteLine(notification.Text);
            Console.WriteLine(notification.Level);
            Console.ResetColor();
        }
        public void Run()
        {
            NotificationBuilder builder = new NotificationBuilder();
            Director director = new Director();
            director.ErrorNotification(builder, "Vlatka");
            ConsoleNotification errorNotification = builder.Build();
            Display(errorNotification);
        }
    }
}

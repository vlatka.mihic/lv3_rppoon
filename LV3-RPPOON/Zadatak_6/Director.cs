﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON.Zadatak_6
{
    class Director
    {
        public void ErrorNotification(IBuilder builder, String author)
        {
            builder.SetAuthor(author);
            builder.SetColor(ConsoleColor.DarkRed);
            builder.SetLevel(Category.ERROR);
            builder.SetTitle("OVO JE PORUKA S POGRESKOM!!!!");
            builder.SetTime(DateTime.Now);
        }

        public void AlertNotification(IBuilder builder, String author)
        {
            builder.SetAuthor(author);
            builder.SetColor(ConsoleColor.Red);
            builder.SetLevel(Category.ALERT);
            builder.SetTitle("OVO JE PORUKA S UPOZORENJEM!!!!");
            builder.SetTime(DateTime.Now);
        }

        public void InfoNotification(IBuilder builder, String author)
        {
            builder.SetAuthor(author);
            builder.SetColor(ConsoleColor.Green);
            builder.SetLevel(Category.INFO);
            builder.SetTitle("Ovo je poruka koja nosi informaciju.");
            builder.SetTime(DateTime.Now);
        }
    }
}

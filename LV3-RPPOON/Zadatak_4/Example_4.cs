﻿using System;

namespace LV3_RPPOON.Zadatak_4
{
    public enum Category { ERROR, ALERT, INFO }
    class Example_4: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            String author = "Vlatka Mihic";
            String title = "LV3-Zadatak4";
            String text = "Napisati program za testiranje funkcionalnosti primjera 3.";
            DateTime timestamp = DateTime.Now;
            Category level = Category.INFO;
            ConsoleColor color = ConsoleColor.Green;

            ConsoleNotification notification = new ConsoleNotification(author, title, text, timestamp, level, color);

            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(notification);


        }
    }
}

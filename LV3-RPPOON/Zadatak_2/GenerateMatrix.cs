﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_RPPOON.Zadatak_2
{
    class GenerateMatrix
    {
        static GenerateMatrix instance;
        private Random random;

        private GenerateMatrix()
        {
            this.random = new Random();
        }
        public static GenerateMatrix GetInstance()
        {
            if(instance == null)
            {
                instance = new GenerateMatrix();
            }
            return instance;
        }
        public double[][] NextMatrix(int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            for (int i = 0; i < rows; i++)
            {
                matrix[i] = new double[columns];
                for (int j = 0; j < columns; j++)
                {
                    matrix[i][j] = random.NextDouble();
                }
            }
            return matrix;
        }

    }
}

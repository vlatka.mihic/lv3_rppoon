﻿using System;

/*
Kreirajte singleton koji predstavlja generator matrica. Klasa treba pružiti metodu kojoj se predaju dimenzije
matrice (broj redaka i broj stupaca) i koja vraća matricu popunjenu realnim pseudo-slučajnim brojevima iz
intervala [0,1). Koliko odgovornosti ima navedena metoda?

    Dvije, pruža mogućnost stvaranja matrice te brine o svom instanciranju i održavanju jedne instance.
*/


namespace LV3_RPPOON.Zadatak_2
{
    class Example_2: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        static void Print(double[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(string.Format("{0}\t", matrix[i][j].ToString()));
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }
        public void Run()
        {
            GenerateMatrix matrix = GenerateMatrix.GetInstance();

            Console.WriteLine("Unesite broj redaka i broj stupaca matrice: ");

            int rows = Convert.ToInt32(Console.ReadLine());
            int columns = Convert.ToInt32(Console.ReadLine());

            Print(matrix.NextMatrix(rows, columns));
        }
    }
}
